function CreateTable () {
	//Если открываем окно, то не перерисовываем объект
	const state = store.getState();
	if (state.app_state === enum_state_app[3]) return;
	
	const container = document.getElementsByClassName("table-organization")[0];
	container.innerHTML = '';

	const list_organization = state.notes;
	if(!list_organization) return;

	const tbl = document.createElement('table');
	container.appendChild(tbl);

	const tbdy = document.createElement('tbody');
	tbl.appendChild(tbdy);
	
	create_header_table();
	list_organization.forEach(org => create_row(org));
	
	function create_header_table () {
		const headers = ['№', 'Название', 'Адрес', 'Владелец', 'Организация обслуживания'];
		const tr = document.createElement('tr');
		tbdy.appendChild(tr);
		
		headers.forEach(item => {
			const elemet = document.createElement('th');
			elemet.innerHTML = item;
			tr.appendChild(elemet);
		});
	}

	function create_row (organization) {
		const tr = document.createElement('tr');
		tr.onclick = () => store.dispatch({type: 'OPEN_PANEL', payload: organization});
		tbdy.appendChild(tr);
		create_cell(tr, organization.id);
		create_cell(tr, organization.title);
		create_cell(tr, organization.address);
		create_cell(tr, organization.owner);
		create_cell(tr, organization.service_organization);
	}

	function create_cell(element, text){
		const td = document.createElement('td');
		td.innerHTML = text;
		element.appendChild(td);
	}
}

store.subscribe(CreateTable);