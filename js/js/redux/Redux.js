function createStore(root_reducer, initialState) {
  let state = root_reducer(initialState, {type: "__INIT__"})
  const subscribers = []

  return {
      dispatch: action => { 
        state = root_reducer(state, action)
        subscribers.forEach(sub => sub());
      },
      subscribe (callback) {
        subscribers.push(callback)
      },
      getState: () => state,
  }
}