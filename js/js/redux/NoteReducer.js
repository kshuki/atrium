const enum_state_app = [
  'init',
  'move_note',
  'update',
  'open_panel',
]

function NoteReducer(state, action) {
  switch (action.type) {
    case 'LOAD_NOTES':
      return {
        ...state,
        app_state: enum_state_app[0],
        notes: action.payload,
        current_note: undefined,
      }

    case 'OPEN_PANEL':
      return {
        ...state,
        app_state: enum_state_app[3],
        current_note: action.payload,
      }

    case 'CHANGE_NOTE':
      return {
        ...state,
        app_state: enum_state_app[2],
        notes: ChangeNote(state.notes, action.payload),
        current_note: undefined,
      }

    case 'MOVE_POINT':
      return {
        ...state,
        app_state: enum_state_app[1],
        notes: ChangeNote(state.notes, action.payload),
        current_note: undefined,
      }

    default:
      return state
  }
}


function ChangeNote(notes = [], note) {
  const new_notes = notes.filter(el => el.id !== note.id);
  if (new_notes.length != notes.length) {
    new_notes.push(note);
  }
  return new_notes;
}


const initialNotes = []

const store = createStore(NoteReducer, initialNotes)

//TODO: Нужно для прочтения сосотояния
store.subscribe(() => console.log(store.getState()))