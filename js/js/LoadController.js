var list_organization = [];

function LoadFile (input) {
	const file = input.files[0];
	const reader = new FileReader();

	reader.readAsText(file);

	reader.onload = function () {
		try {
			data = JSON.parse(reader.result);
			let list_organization = data.list_organization;
			
			store.dispatch({type: 'LOAD_NOTES', payload: list_organization})
		} catch(e) {
			alert(e);
		}
	};

	reader.onerror = function () {
		console.log(reader.error);
	};
}