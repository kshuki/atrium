var map;

// Инициализация и уничтожение карты при нажатии на кнопку.
function InitMap () {
	if (map) {
		return;
	}
	// Создание экземпляра карты и его привязка к контейнеру с
	// заданным id ("map").
	map = new ymaps.Map('map', {
			// При инициализации карты обязательно нужно указать
			// её центр и коэффициент масштабирования.
			center: [55.76, 37.64], // Москва
			zoom: 10,
			controls: []
	});
}


function UpdatePointers () {
	// Дождёмся загрузки API и готовности DOM.
	if(!map) {
		ymaps.ready(() => {
			InitMap()
			UpdatePointers()
		});
		return;
	}

	const state = store.getState();

	//Чтобы не перерисовывать лищний раз, смотрим на состояние приложенияж
	if (state.app_state == enum_state_app[1]) return;
	
	//Удалаяем старные местки
	map.geoObjects.removeAll()
	//Создаем новые метки
	state.notes.forEach(note => {
		const placemark = new ymaps.Placemark(note.address, {
			balloonContent: `адрес: ${note.address}<br/>Владелец: <strong>${note.owner}</strong>`,
			iconCaption: note.title,
		}, {
			preset: 'islands#violetDotIconWithCaption',
			iconColor: '#3caa3c',
			draggable: true
		})

		placemark.events.add('dragend', function(e) {
			let coords = placemark.geometry.getCoordinates()
			store.dispatch({type: 'MOVE_POINT', payload: {...note, address: coords}})
		})

		map.geoObjects.add(placemark)
	});
}

store.subscribe(() => ymaps.ready(UpdatePointers));