function OpenEditPanel () {
	console.log('test');
	const state = store.getState();
	if (state.app_state !== enum_state_app[3]) return;

	const panel = document.getElementById('edit-panel');
	panel.style.display = 'flex';

	const form = document.getElementById('edit-form');
	
	const organization = state.current_note;
	console.log(organization.address[1]);
	form.title.value = organization.title;
	form.latitude.value = organization.address[0];
	form.longitude.value = organization.address[1];
	form.owner.value = organization.owner;
	form.service_organization.value = organization.service_organization;
	
	form.onsubmit = (event) => { 
		event.preventDefault();
		CloseEditPanel();
		store.dispatch({type: 'CHANGE_NOTE', 
			payload: {
				...organization, 
				title: form.title.value,
				address: [Number(form.latitude.value), Number(form.longitude.value)],
				owner: form.owner.value,
				service_organization: form.service_organization.value
			}})

	};
}

function CloseEditPanel (event) {
	if (event) {
		event.preventDefault();
	}

	const panel = document.getElementById('edit-panel');
	panel.style.display = 'none';
	return false;
}

store.subscribe(OpenEditPanel);